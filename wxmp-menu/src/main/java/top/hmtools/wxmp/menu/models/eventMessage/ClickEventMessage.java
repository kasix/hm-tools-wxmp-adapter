package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * 点击菜单拉取消息时的事件推送 <br>
 *  数据包示例：
 * <p>
 * {@code 
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[CLICK]]></Event>
<EventKey><![CDATA[EVENTKEY]]></EventKey>
</xml>
 * }
 * </p>
 * @author Hybomyth
 *
 */
public class ClickEventMessage extends BaseEventMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6092023794768222411L;

	@Override
	public String toString() {
		return "ClickEventMessage [Event=" + event + ", EventKey=" + eventKey + ", ToUserName=" + toUserName
				+ ", FromUserName=" + fromUserName + ", CreateTime=" + createTime + ", MsgType=" + msgType + ", MsgId="
				+ msgId + "]";
	}

	@Override
	public void configXStream(XStream xStream) {
	}

	
}

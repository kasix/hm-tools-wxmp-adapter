package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * pic_weixin：弹出微信相册发图器的事件推送
 * <br>
 * <p>
 * <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
<CreateTime>1408090816</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[pic_weixin]]></Event>
<EventKey><![CDATA[6]]></EventKey>
<SendPicsInfo><Count>1</Count>
<PicList><item><PicMd5Sum><![CDATA[5a75aaca956d97be686719218f275c6b]]></PicMd5Sum>
</item>
</PicList>
</SendPicsInfo>
</xml>
 * </p>
 * @author Hybomyth
 *
 */
public class PicWeixinEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9085581793136687123L;

	/**
	 * 发送的图片信息
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * 发送的图片信息
	 * @return
	 */
	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	/**
	 * 发送的图片信息
	 * @param sendPicsInfo
	 */
	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		xStream.alias("item", SendPicItem.class);
	}

	@Override
	public String toString() {
		return "PicWeixinEventMessage [sendPicsInfo=" + sendPicsInfo + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
	
}

package top.hmtools.wxmp.menu.apis;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.menu.models.conditional.ConditionalBean;
import top.hmtools.wxmp.menu.models.conditional.ConditionalMenuBean;
import top.hmtools.wxmp.menu.models.conditional.MatchruleBean;
import top.hmtools.wxmp.menu.models.conditional.TryMatchParamBean;
import top.hmtools.wxmp.menu.models.simple.Button;
import top.hmtools.wxmp.menu.models.simple.MenuBean;
import top.hmtools.wxmp.menu.models.simple.MenuWapperBean;

public class IConditionalMenuApiTest {
	
	protected WxmpSession wxmpSession;
	private IConditionalMenuApi conditionalMenuApi ;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		conditionalMenuApi = this.wxmpSession.getMapper(IConditionalMenuApi.class);
	}

	@Test
	public void testaddConditional() {
		ConditionalMenuBean conditionalMenuBean = JMockData.mock(ConditionalMenuBean.class);
		
String baseUrl = "hm.hn.cn";
		
		//底部第一个主按钮
		Button bbGuanwang = new Button();
		bbGuanwang.setName("官网").setType("view").setUrl("http://m.hybo.net/main/index/index.html");
		
		Button bbDongTai = new Button();
		bbDongTai.setName("动态").setType("view").setUrl("http://"+baseUrl+"/main/news/index.html");
		
		Button bbVideo = new Button();
		bbVideo.setName("视频").setType("view").setUrl("http://"+baseUrl+"/main/news/video.html");
		
		Button buttonBeanIndex = new Button();
		buttonBeanIndex.setName("哈哈");
		buttonBeanIndex.addSubButton(bbGuanwang,bbDongTai);
		
		conditionalMenuBean.setButton(new ArrayList<Button>());
		conditionalMenuBean.addButton(buttonBeanIndex);
		
		MatchruleBean matchrule = new MatchruleBean();
		matchrule.setCity("广州");
		matchrule.setClient_platform_type("2");
		matchrule.setCountry("中国");
		matchrule.setLanguage("zh_CN");
		matchrule.setProvince("广东");
		matchrule.setSex("1");
		matchrule.setTag_id("2");
		conditionalMenuBean.setMatchrule(matchrule);
		
		ConditionalBean conditionalBean = conditionalMenuApi.addConditional(conditionalMenuBean);
		System.out.println(conditionalBean);
	}
	
	
	@Test
	public void testdelConditional(){
		ConditionalBean conditionalBean = new ConditionalBean();
		conditionalBean.setMenuid("480747868");
		ErrcodeBean delConditional = this.conditionalMenuApi.delConditional(conditionalBean);
		System.out.println(delConditional);
	}
	
	
	@Test
	public void testtryMatch(){
		TryMatchParamBean tryMatchParamBean = new TryMatchParamBean();
		tryMatchParamBean.setUser_id("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		MenuWapperBean tryMatch = this.conditionalMenuApi.tryMatch(tryMatchParamBean);
		System.out.println(tryMatch);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public interface WxmpSessionFactory {
	
	public WxmpSession openSession();

	public WxmpConfiguration getWxmpConfiguration();
}

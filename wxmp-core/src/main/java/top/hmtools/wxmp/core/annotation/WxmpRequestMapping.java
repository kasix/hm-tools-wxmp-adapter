package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 本注解参照spring MVC中@requestMapping注解实现，使用时须结合 {@link top.hmtools.wxmp.core.annotation.WxmpController}，以达到接收到
 * 微信事件通知消息后使用相应的方法处理消息
 * @author HyboWork
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpRequestMapping {

	
}

package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 消息类实体类注解
 * <br>被此注解修饰的类，可以被尝试从微信侧接收到的xml数据包中获取数据
 * @author HyboWork
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpMessage {

	/**
	 * 消息类型
	 * @return
	 */
	MsgType msgType();

	/**
	 * 事件类型
	 * @return
	 */
	Event event() default Event.none;
}

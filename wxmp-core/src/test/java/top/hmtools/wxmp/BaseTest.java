package top.hmtools.wxmp;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class BaseTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected WxmpSession wxmpSession;
	
	protected WxmpSessionFactory factory;

	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
	}
}

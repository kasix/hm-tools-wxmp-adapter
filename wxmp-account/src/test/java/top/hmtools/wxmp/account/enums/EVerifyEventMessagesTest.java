package top.hmtools.wxmp.account.enums;

import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.model.message.BaseMessage;

public class EVerifyEventMessagesTest {

	@Test
	public void test() {
		EVerifyEventMessages[] values = EVerifyEventMessages.values();
		for(EVerifyEventMessages item:values){
			BaseMessage message = (BaseMessage)JMockData.mock(item.getClassName());
			System.out.println(message);
			System.out.println(message.toXmlMsg());
		}
	}
	
}

package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 卡券消息（注意图文消息的media_id需要通过上述方法来得到）：
 * 
 * @author HyboWork
 *
 */
public class WxcardTagGroupSendParam extends BaseTagGroupSendParam {

	private Wxcard wxcard;

	public Wxcard getWxcard() {
		return wxcard;
	}

	public void setWxcard(Wxcard wxcard) {
		this.wxcard = wxcard;
	}

	@Override
	public String toString() {
		return "WxcardTagGroupSendParam [wxcard=" + wxcard + ", filter=" + filter + ", msgtype=" + msgtype + "]";
	}

}

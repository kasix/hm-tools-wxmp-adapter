package top.hmtools.wxmp.message.template.model;

import java.util.Date;

/**
 * Auto-generated: 2019-08-29 11:48:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DataItem {

	private String value;
	private Date color;

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setColor(Date color) {
		this.color = color;
	}

	public Date getColor() {
		return color;
	}

}
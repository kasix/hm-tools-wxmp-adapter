package top.hmtools.wxmp.message.template.model;

import java.util.List;

/**
 * Auto-generated: 2019-08-29 11:22:9
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TemplateListResult {

	private List<Template> template_list;

	public void setTemplate_list(List<Template> template_list) {
		this.template_list = template_list;
	}

	public List<Template> getTemplate_list() {
		return template_list;
	}

}
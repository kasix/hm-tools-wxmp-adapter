package top.hmtools.wxmp.message.group.model.event;

import java.util.ArrayList;
import java.util.List;

public class ResultListType {

	protected List<ItemType> item;

	public List<ItemType> getItem() {
		if (item == null) {
			item = new ArrayList<ItemType>();
		}
		return this.item;
	}

	public void setItem(List<ItemType> item) {
		this.item = item;
	}

}
